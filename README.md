# Interface du moteur de recherche #

L'interface auparavant contenu dans le noyau d'Ovidentia a été externalisé dans un module pour la rendre "remplaçable". Ce module est donc distribué dans le package de version d'Ovidentia. à partir de la version 8.2.90

## Changelog ##

### Version 0.6.4 - 20/08/2018 ###

* Lors de la recherche dans un annuaire, les trois select du formulaire de recherche dans des champs spécifiques de l'annuaire ont désormais des options vides
* Ajout de la possibilité de configurer quels champs des trois select du formulaire de recherche dans des champs spécifiques de l'annuaire sont selectionnés par défaut.
  La configuration des champs se fait dans le fichier config.php d'Ovidentia.
* Pour configurer quels champs sont sélectionnés par défaut, il faut ajouter dans le config.php d'Ovidentia les lignes suivantes :

define('/search/directoryDefaultSearchFields/select1', 'email'); //Le premier select a l'option Email selectionnée

define('/search/directoryDefaultSearchFields/select2', 'sn'); //Le second select a l'option Nom selectionnée

define('/search/directoryDefaultSearchFields/select3', 'title'); //Le troisième select a l'option Titre selectionnée

* Le second paramètre du define peut être une chaine vide pour selectionner l'option vide :

define('/search/directoryDefaultSearchFields/select1', '');

* Si aucune configuration spécifique n'est présente dans le fichier config.php d'Ovidentia, alors l'option sélectionnée du premier select est la première option du select. Les deux autres selects ont l'option vide sélectionnée.