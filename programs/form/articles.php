<?php
/************************************************************************
 * OVIDENTIA http://www.ovidentia.org                                   *
 ************************************************************************
 * Copyright (c) 2003 by CANTICO ( http://www.cantico.fr )              *
 *                                                                      *
 * This file is part of Ovidentia.                                      *
 *                                                                      *
 * Ovidentia is free software; you can redistribute it and/or modify    *
 * it under the terms of the GNU General Public License as published by *
 * the Free Software Foundation; either version 2, or (at your option)  *
 * any later version.													*
 *																		*
 * This program is distributed in the hope that it will be useful, but  *
 * WITHOUT ANY WARRANTY; without even the implied warranty of			*
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.					*
 * See the  GNU General Public License for more details.				*
 *																		*
 * You should have received a copy of the GNU General Public License	*
 * along with this program; if not, write to the Free Software			*
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,*
 * USA.																	*
************************************************************************/

require_once $GLOBALS['babInstallPath'].'utilit/searchrealmsincl.php';


class search_Articles_SearchTemplate extends bab_SearchTemplate {

    private $arrtopicscategories = null;
    private $authors = null;
    
    public function __construct() {
    
        global $babBody;
        global $babDB;
    
        include_once $GLOBALS['babInstallPath'].'utilit/topincl.php';
        include_once $GLOBALS['babInstallPath'].'utilit/userinfosincl.php';
    
        $babBody->addJavascriptFile($GLOBALS['babScriptPath'].'bab_dialog.js');
    
        $this->t_topic 	= search_translate('Topic');
        $this->t_all	= search_translate('All');
        $this->t_author = search_translate('Author');
        $this->t_after 	= search_translate('After date');
        $this->t_before	= search_translate('Before date');
    
        $this->after 	= bab_toHtml(bab_rp('after'));
        $this->before 	= bab_toHtml(bab_rp('before'));
    
        $delegation = bab_rp('delegation', null);
        $id_dgowner = false;
        if (null !== $delegation && 'DGAll' !== $delegation)
        {
            include_once $GLOBALS['babInstallPath'].'utilit/delegincl.php';
            $arr = bab_getUserVisiblesDelegations();
            if (isset($arr[$delegation]))
            {
                $id_dgowner = $arr[$delegation]['id'];
            }
            $id_dgowner = $delegation;
        }
    
        $this->arrtopicscategories = bab_getArticleTopicsAsTextTree(0, $id_dgowner);
    
    
    
        $this->authors = $babDB->db_query('
                SELECT
                u.id,
                u.firstname,
                u.lastname
                FROM
                '.BAB_USERS_TBL.' u,
                '.BAB_ARTICLES_TBL.' a
                WHERE
                '.bab_userInfos::queryAllowedUsers('u').'
                AND a.id_author=u.id
                AND a.id_topic IN('.$babDB->quote(bab_getUserIdObjects(BAB_TOPICSVIEW_GROUPS_TBL)).')
    
                GROUP BY a.id_author ORDER BY u.lastname, u.firstname
                ');
    
    }
    
    
    /**
     * Template method
     */
    public function getnextauthor() {
        global $babDB;
    
        if ($arr = $babDB->db_fetch_assoc($this->authors)) {
            $this->option = bab_toHtml($arr['lastname'].' '.$arr['firstname']);
            $this->value = bab_toHtml($arr['id']);
    
            $this->selected = $arr['id'] === bab_rp('a_authorid');
    
            return true;
        }
    
        return false;
    }
    
    
    
    /**
     * Template method
     */
    public function getnexttopiccategory()  {
    
        if (list(,$arr) = each($this->arrtopicscategories)) {
    
            $type = $arr['category'] ? 'category-' : 'topic-';
            $value = $type.$arr['id_object'];
    
            $this->option 	= bab_toHtml(str_repeat(bab_nbsp(),3).$arr['name']);
            $this->value 	= bab_toHtml($value);
    
            $this->selected = $value === bab_rp('a_topiccategory');
    
            return true;
        }
    
        return false;
    }
    
}
