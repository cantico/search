<?php
/************************************************************************
 * OVIDENTIA http://www.ovidentia.org                                   *
 ************************************************************************
 * Copyright (c) 2003 by CANTICO ( http://www.cantico.fr )              *
 *                                                                      *
 * This file is part of Ovidentia.                                      *
 *                                                                      *
 * Ovidentia is free software; you can redistribute it and/or modify    *
 * it under the terms of the GNU General Public License as published by *
 * the Free Software Foundation; either version 2, or (at your option)  *
 * any later version.													*
 *																		*
 * This program is distributed in the hope that it will be useful, but  *
 * WITHOUT ANY WARRANTY; without even the implied warranty of			*
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.					*
 * See the  GNU General Public License for more details.				*
 *																		*
 * You should have received a copy of the GNU General Public License	*
 * along with this program; if not, write to the Free Software			*
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,*
 * USA.																	*
************************************************************************/

require_once $GLOBALS['babInstallPath'].'utilit/searchrealmsincl.php';


class search_Directories_SearchTemplate extends bab_SearchTemplate {

	private $directories = null;
	private $fields = null;

	public function __construct($realm) 
		{
		global $babDB;
		
		$delegation = bab_rp('delegation', null);
		$id_dgowner = false;
		if (null !== $delegation && 'DGAll' !== $delegation)
		{
			include_once $GLOBALS['babInstallPath'].'utilit/delegincl.php';
			$arr = bab_getUserVisiblesDelegations();
			if (isset($arr[$delegation]))
			{
				$id_dgowner = $arr[$delegation]['id'];
			}
			$id_dgowner = str_replace('DG', '', $delegation);
		}

		$this->directories = bab_getUserDirectories(true, $id_dgowner);
		$this->fields[] = new bab_SearchField();

		foreach($realm->getFields() as $field) {
			if ($field->searchable()) {
				$this->fields[] = $field;
			}
		}

		$this->search_form_folded = (bab_rp('idx') === 'find' ? '1' : '0');
		$this->t_filter_by_directory = search_translate('Filter search results by a directory :');
		$this->t_all = search_translate('All');
		$this->t_search_in_specific_field = search_translate('Search in a specific field :');

		}


	/**
	 * Template method
	 */
	public function selectname() 
		{

		if( list($i,$field) = each($this->fields))
			{
			$this->selindex = $i;
			$this->name = bab_toHtml($field->getName());
			$this->description = bab_toHtml($field->getDescription());
			$this->descriptionJs = bab_toHtml($field->getDescription(), BAB_HTML_ENTITIES | BAB_HTML_JS);

			$this->isnotcustom = 0 !== mb_strpos($field->getName(), 'babdirf');

			$dirselect = bab_rp('f');
			$dirfield = bab_rp('v');

			$this->fieldvalue = isset($dirfield[$this->j]) ? bab_toHtml($dirfield[$this->j]) : '';
			if ( isset($dirselect[$this->j]) && $dirselect[$this->j] == $field->getName())
				$this->selected = "selected";
			else {

		        $select1 = bab_Registry::get('/search/directoryDefaultSearchFields/select1', $this->fields[1]->getName());
		        $select2 = bab_Registry::get('/search/directoryDefaultSearchFields/select2');
		        $select3 = bab_Registry::get('/search/directoryDefaultSearchFields/select3');
		        
		        if($this->j == 0 && $field->getName() == $select1){
		            $this->selected = "selected";
		        }
		        elseif($this->j == 1 && $field->getName() == $select2){
		            $this->selected = "selected";
		        }
		        elseif($this->j == 2 && $field->getName() == $select3){
		            $this->selected = "selected";
		        }
		        else{
		            $this->selected = false;
		        }

			}

			return true;
			}
		else
			{
			reset($this->fields);
			return false;
			}
		}






	/**
	 * Template method
	 */
	public function getnextfield()
		{
		static $k = 0;
		if( $k < $this->countfieldsfromdir)
			{
			$this->fieldnamefromdir = "babdirf".$this->tblxfields[$k]['name'] ;
			$this->name = "babdirf".$this->tblxfields[$k]['name'];
			$this->description = $this->tblxfields[$k]['description'];
			$this->fieldindex = $k;
			if ( isset($this->fields['f['.$this->j.']']) && $this->fields['f['.$this->j.']'] == $this->name)
				{
				$this->selected = "selected";
				}
			else
				{
				$this->selected = false;
				}
			$k++;
			return true;
			}
		else
			{
			$k = 0;
			return false;
			}
		}


	/**
	 * Template method
	 */
	public function getnextfieldtosearch() 
		{
		static $j = 0;
		if( $j < FIELDS_TO_SEARCH)
			{
			$this->fieldcounter = $j;

			$dirfield = bab_rp('v');
			
			$this->value = isset($dirfield[$j]) ? $dirfield[$j] : '';
			$this->j = $j;
			$j++;
			return true;
			}
		else
			{
			$j = 0;
			return false;}
		}


	/**
	 * Template method
	 */
	public function getnextdir() 
		{
		global $babDB;
		if(list(, $arr) = each($this->directories))
			{
			
			$this->topicid = $arr['id'];
			$this->selected = bab_rp('directoryid') == $arr['id'];

			$this->topictitle = bab_toHtml($arr['name']);

			$req = "select df.id, dfd.name from ".BAB_DBDIR_FIELDS_DIRECTORY_TBL." dfd left join ".BAB_DBDIR_FIELDSEXTRA_TBL." df ON df.id_directory=dfd.id_directory and df.id_field = ( ".BAB_DBDIR_MAX_COMMON_FIELDS." + dfd.id ) where df.id_directory='".(($arr['id_group']==0) ? $arr['id'] : 0)."'";
			$res = $babDB->db_query($req);

			$lk = 0;
			while ($arr = $babDB->db_fetch_array($res))
				{
				$tblxn[$lk] = $arr['id'];
				$tblxd[$lk] = translateDirectoryField($arr['name']);
				$lk++;
				}

			$this->tblxfields = array();
			$this->countfieldsfromdir = 0;
			if( $lk > 0 )
				{
				
				$fliped = array_flip($tblxd);
				bab_sort::ksort($fliped);
				$lk= 0;
				foreach($fliped as $value => $key)
					{
					$this->tblxfields[$lk]['name'] = $tblxn[$key];
					$this->tblxfields[$lk]['description'] = $value;
					$lk++;
					}
				$this->countfieldsfromdir = count($this->tblxfields);
				}
			

			return true;
			}
		else
			{
			reset($this->directories);
			return false;
			}
		}
}
