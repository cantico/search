<?php
/************************************************************************
 * OVIDENTIA http://www.ovidentia.org                                   *
 ************************************************************************
 * Copyright (c) 2003 by CANTICO ( http://www.cantico.fr )              *
 *                                                                      *
 * This file is part of Ovidentia.                                      *
 *                                                                      *
 * Ovidentia is free software; you can redistribute it and/or modify    *
 * it under the terms of the GNU General Public License as published by *
 * the Free Software Foundation; either version 2, or (at your option)  *
 * any later version.													*
 *																		*
 * This program is distributed in the hope that it will be useful, but  *
 * WITHOUT ANY WARRANTY; without even the implied warranty of			*
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.					*
 * See the  GNU General Public License for more details.				*
 *																		*
 * You should have received a copy of the GNU General Public License	*
 * along with this program; if not, write to the Free Software			*
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,*
 * USA.																	*
************************************************************************/


require_once $GLOBALS['babInstallPath'].'utilit/tagApi.php';

function search_getTagForm()
{
    
    $tags = bab_getInstance('bab_TagMgr');
    /*@var $tags bab_TagMgr */
    
    $display = array();
    $i = 0;
    $maxrefcount = 0;
    
    foreach($tags->selectRefCount() as $tag) {
    
        if ($i > 50) {
            break;
        }
    
        $refcount = $tag->getRefCount();
        if ($maxrefcount < $refcount) {
            $maxrefcount = $refcount;
        }
    
        $display[strtolower($tag->getName())] = $tag;
        $i++;
    }
    
    bab_sort::ksort($display);
    
    $minsize = 9;
    $maxsize = 15;
    
    $url = bab_SearchRealmTags::getUrl();
    
    $html = '<div class="tag_cloud">';
    foreach($display as $tag) {
        $size = $minsize + (($maxsize * $tag->getRefCount()) / $maxrefcount);
    
        $html .= bab_sprintf(' <a href="%s" style="font-size:%spx">%s</a> ',
            bab_toHtml(bab_url::mod($url, 'what', $tag->getName())),
            round($size),
            bab_toHtml($tag->getName())
        );
    }
    $html .= '</div>';
    
    return $html;
}
