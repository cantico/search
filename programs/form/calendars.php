<?php
/************************************************************************
 * OVIDENTIA http://www.ovidentia.org                                   *
 ************************************************************************
 * Copyright (c) 2003 by CANTICO ( http://www.cantico.fr )              *
 *                                                                      *
 * This file is part of Ovidentia.                                      *
 *                                                                      *
 * Ovidentia is free software; you can redistribute it and/or modify    *
 * it under the terms of the GNU General Public License as published by *
 * the Free Software Foundation; either version 2, or (at your option)  *
 * any later version.													*
 *																		*
 * This program is distributed in the hope that it will be useful, but  *
 * WITHOUT ANY WARRANTY; without even the implied warranty of			*
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.					*
 * See the  GNU General Public License for more details.				*
 *																		*
 * You should have received a copy of the GNU General Public License	*
 * along with this program; if not, write to the Free Software			*
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,*
 * USA.																	*
************************************************************************/

require_once $GLOBALS['babInstallPath'].'utilit/searchrealmsincl.php';




/**
 * @package search
 */
class search_Calendar_SearchTemplate extends bab_SearchTemplate {

	private $rescal;


	public function __construct() {
		global $babDB;
		global $babBody;
		include_once $GLOBALS['babInstallPath']."utilit/calincl.php";
		include_once $GLOBALS['babInstallPath'].'utilit/dateTime.php';
		
		$babBody->addJavascriptFile($GLOBALS['babScriptPath'].'bab_dialog.js');
		
		
		$delegation = bab_rp('delegation', null);
		$id_dgowner = null;
		if (null !== $delegation && 'DGAll' !== $delegation)
		{
			include_once $GLOBALS['babInstallPath'].'utilit/delegincl.php';
			$arr = bab_getUserVisiblesDelegations();
			if (isset($arr[$delegation]))
			{
				$id_dgowner = $arr[$delegation]['id'];
			}
			$id_dgowner = str_replace('DG', '', $delegation);
		}

		$this->rescal = bab_getICalendars()->getCalendars($id_dgowner);
		bab_sort::sortObjects($this->rescal, 'getName');
		
		$this->t_calendar = search_translate('Calendar');
		$this->t_all 	= search_translate('All');
		$this->t_after 	= search_translate('After');
		$this->t_before = search_translate('Before');
		$this->t_calendar_boundaries_mandatory = search_translate('The dates search boundaries are mandatory');
		
		
		
		$after = BAB_DateTime::fromUserInput(bab_rp('after'));
		
		if (!$after) {
			$after = BAB_DateTime::now();
			$after->less(1, BAB_DATETIME_DAY);
		}
		
		if ($before = BAB_DateTime::fromUserInput(bab_rp('before'))) {
			$before->add(1, BAB_DATETIME_DAY);
			
		} else {
			$before = BAB_DateTime::now();
			$before->add(1, BAB_DATETIME_DAY);
		}

		
		

		$this->after 	= date('d-m-Y', $after->getTimeStamp());
		$this->before 	= date('d-m-Y', $before->getTimeStamp());
	}

	public function getnextcal() {

		if (list(, $calendar) = each($this->rescal)) {
			$this->value = bab_toHtml($calendar->getUrlIdentifier());
			$this->option = bab_toHtml($calendar->getName());
			$this->selected = $calendar->getUrlIdentifier() == bab_rp('h_calendar');
			return true;
		}

		return false;
	}
	
}



