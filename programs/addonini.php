;<?php /*

[general]
name							="search"
version							="0.6.4"
addon_type						="EXTENSION"
encoding						="UTF-8"
mysql_character_set_database	="latin1"
description						="Search UI"
description.fr					="Module fournissant l'interface du moteur de recherche"
delete							="1"
ov_version						="8.6"
php_version						="5.1.0"
addon_access_control			="0"
author							="Cantico"
mysql_character_set_database	="latin1,utf8"
icon							="icon.png"
tags                            ="extension,search,default"

[addons]

LibTranslate				    =">=1.12.0rc3.01"

;*/