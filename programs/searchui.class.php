<?php
/************************************************************************
 * OVIDENTIA http://www.ovidentia.org                                   *
 ************************************************************************
 * Copyright (c) 2003 by CANTICO ( http://www.cantico.fr )              *
 *                                                                      *
 * This file is part of Ovidentia.                                      *
 *                                                                      *
 * Ovidentia is free software; you can redistribute it and/or modify    *
 * it under the terms of the GNU General Public License as published by *
 * the Free Software Foundation; either version 2, or (at your option)  *
 * any later version.													*
 *																		*
 * This program is distributed in the hope that it will be useful, but  *
 * WITHOUT ANY WARRANTY; without even the implied warranty of			*
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.					*
 * See the  GNU General Public License for more details.				*
 *																		*
 * You should have received a copy of the GNU General Public License	*
 * along with this program; if not, write to the Free Software			*
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,*
 * USA.																	*
************************************************************************/

require_once dirname(__FILE__).'/functions.php';
require_once $GLOBALS['babInstallPath'].'utilit/urlincl.php';
require_once dirname(__FILE__).'/utilit/searchincl.php';

bab_functionality::includeOriginal('SearchUi');

class Func_SearchUi_Default extends Func_SearchUi
{
    public function getDescription()
    {
        return search_translate('Default interface');
    }
    

    
    /**
     * Mandatory method to get the serach interface
     * @param string $realmName Optional search realm name
     * @param string $keyword Optional search keyword(s) to initialize search results
     * @return string
     */
    public function getUrl($realmName = null, $keyword = null)
    {
        
        $addon = bab_getAddonInfosInstance('search');
        
        if (!$addon) {
            return null;
        }
        
        if (!isset($realmName)) {
            return $addon->getUrl().'main';
        }
        
        $url = new bab_url($addon->getUrl().'main');
        $url->item = $realmName;
        
        return $url->toString();
    }
    
    
    
    /**
     * Get TG value for the search functionality (search form page)
     * @return string
     */
    public function getTg()
    {
        return 'addon/search/main';
    }
    
    
    
    
    
    /**
     * Get search form as HTML string
     * @param string bab_SearchRealm
     * @return string
     */
    public function getSearchFormHtml(bab_SearchRealm $realm = null)
    {
        
        $addon = bab_getAddonInfosInstance('search');
        
        require_once dirname(__FILE__).'/utilit/searchincl.php';
        $html = search_DefaultForm::getHTML();
        
        switch(true) {
            case $realm instanceof bab_SearchRealmDirectories:
                require_once dirname(__FILE__).'/form/directories.php';
                $template = new search_Directories_SearchTemplate($realm);
                $html .= $addon->printTemplate($template, 'search.html', 'directories_form');
                break;
                
            case $realm instanceof bab_SearchRealmArticles:
            case $realm instanceof bab_SearchRealmPublication:
                require_once dirname(__FILE__).'/form/articles.php';
                $template = new search_Articles_SearchTemplate($realm);
                $html .= $addon->printTemplate($template, 'search.html', 'articles_form');
                break;
                
            case $realm instanceof bab_SearchRealmCalendars:
                require_once dirname(__FILE__).'/form/calendars.php';
                $template = new search_Calendar_SearchTemplate($realm);
                $html .= $addon->printTemplate($template, 'search.html', 'calendar_form');
                break;
                
            case $realm instanceof bab_SearchRealmTags:
                require_once dirname(__FILE__).'/form/tags.php';
                $html .= search_getTagForm();
                break;
        }
        
        return $html;
    }
    
    
    /**
     * Create the search form criteria from the submited form result
     * @param bab_SearchRealm $realm
     * @return bab_SearchCriteria
     */
    public function getSearchFormCriteria(bab_SearchRealm $realm)
    {
        
        switch(true) {
            case $realm instanceof bab_SearchRealmDirectories:
                require_once dirname(__FILE__).'/criteria/directories.php';
                $factory = new search_DirectoriesCriteriaFactory($realm);
                return $factory->getSearchFormCriteria();
        
            case $realm instanceof bab_SearchRealmArticles:         // inherited from bab_SearchRealmTopic, no specific criteria
            case $realm instanceof bab_SearchRealmArticlesComments: // inherited from bab_SearchRealmTopic, no specific criteria
            case $realm instanceof bab_SearchRealmTopic:
                require_once dirname(__FILE__).'/criteria/topic.php';
                $factory = new search_TopicCriteriaFactory($realm);
                return $factory->getSearchFormCriteria();
                
            case $realm instanceof bab_SearchRealmArticlesFiles:
                require_once dirname(__FILE__).'/criteria/articlesfiles.php';
                $factory = new search_ArticlesFilesCriteriaFactory($realm);
                return $factory->getSearchFormCriteria();
        
            case $realm instanceof bab_SearchRealmCalendars:
                require_once dirname(__FILE__).'/criteria/calendars.php';
                $factory = new search_CalendarsCriteriaFactory($realm);
                return $factory->getSearchFormCriteria();

        }
        
        require_once dirname(__FILE__).'/utilit/searchincl.php';
        return search_DefaultForm::getCriteria($realm);
    }
    
    
    /**
     * get a criteria without field criterions from a search query made with the form generated with the method <code>getSearchFormHtml()</code>
     * 
     * @param bab_SearchRealm $realm
     * 
     * @return bab_SearchCriteria
     */
    public function getSearchFormFieldLessCriteria(bab_SearchRealm $realm)
    {
        require_once dirname(__FILE__).'/utilit/searchincl.php';
        return search_DefaultForm::getFieldLessCriteria($realm);
    }
    
    
    
    /**
     * Optional article url to the preview
     * @param int $id_article
     * @return string
     */
    public function getArticlePopupUrl($id_article) {
        $addon = bab_getAddonInfosInstance('search');
        $url = new bab_url($addon->getUrl().'main');
        $url->idx = 'articles';
        $url->id = $id_article;
        $url->w = $this->highlightKeyword('publication');
        return $url->toString();
    }
    
    /**
     * Optional contact url to the preview
     * @param int $id_contact
     * @return string
     */
    public function getContactPopupUrl($id_contact) {
        $addon = bab_getAddonInfosInstance('search');
        $url = new bab_url($addon->getUrl().'main');
        $url->idx = 'contacts';
        $url->id = $id_contact;
        $url->w = $this->highlightKeyword('contacts');
        return $url->toString();
    }
    
    /**
     * Optional directory entry url to the preview popup
     * @param int $id_entry
     * @return string
     */
    public function getDirEntryPopupUrl($id_entry) {
        $addon = bab_getAddonInfosInstance('search');
        $url = new bab_url($addon->getUrl().'main');
        $url->idx = 'directories';
        $url->id = $id_entry;
        $url->w = $this->highlightKeyword('directories');
        return $url->toString();
    }
    
    
    /**
     * Optional filemanager file url to the preview popup
     * @param int $id_file
     * @return string
     */
    public function getFilePopupUrl($id_file) {
        $addon = bab_getAddonInfosInstance('search');
        $url = new bab_url($addon->getUrl().'main');
        $url->idx = 'files';
        $url->id = $id_file;
        $url->w = $this->highlightKeyword('files');
        return $url->toString();
    }
    
    
    
    /**
     * Get a string to highlight on a result page
     * @param string $realmName search realm name
     * @return string
     */
    public function highlightKeyword($realmName) {
        $primary_search = bab_rp('what');
        $secondary_search = bab_rp('what2');
    
        if ($secondary_search) {
            return $primary_search.' '.$secondary_search;
        }
    
        return $primary_search;
    }
}
