<?php
/************************************************************************
 * OVIDENTIA http://www.ovidentia.org                                   *
 ************************************************************************
 * Copyright (c) 2003 by CANTICO ( http://www.cantico.fr )              *
 *                                                                      *
 * This file is part of Ovidentia.                                      *
 *                                                                      *
 * Ovidentia is free software; you can redistribute it and/or modify    *
 * it under the terms of the GNU General Public License as published by *
 * the Free Software Foundation; either version 2, or (at your option)  *
 * any later version.													*
 *																		*
 * This program is distributed in the hope that it will be useful, but  *
 * WITHOUT ANY WARRANTY; without even the implied warranty of			*
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.					*
 * See the  GNU General Public License for more details.				*
 *																		*
 * You should have received a copy of the GNU General Public License	*
 * along with this program; if not, write to the Free Software			*
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,*
 * USA.																	*
************************************************************************/

require_once dirname(__FILE__).'/topic.php';

class search_ArticlesFilesCriteriaFactory extends search_TopicCriteriaFactory
{
    


    

    /**
	 * get a criteria from a search query made with the form generated with the method <code>getSearchFormHtml()</code>
	 * @see Func_SearchUi::getSearchFormHtml()
	 * @return bab_SearchCriteria
	 */
	public function getSearchFormCriteria() {
	    
	    $realm = $this->realm;
	    
		// default search fields
		$criteria = parent::getSearchFormCriteria($realm);

		$realm->sql_criteria = new bab_SearchInvariant;
		
		
		$delegation = bab_rp('delegation', null);
		
		if (null !== $delegation && 'DGAll' !== $delegation)
		{
			// if id_dgowner field exist on search real, filter by delegation
			
			require_once dirname(__FILE__).'/delegincl.php';
			$arr = bab_getUserVisiblesDelegations();
			
			if (isset($arr[$delegation]))
			{
				$id_dgowner = $arr[$delegation]['id'];
				$realm->sql_criteria = $realm->sql_criteria->_AND_($realm->id_dgowner->is($id_dgowner));
			}
			$id_dgowner = str_replace('DG', '', $delegation);
			$realm->sql_criteria = $realm->sql_criteria->_AND_($realm->id_dgowner->is($id_dgowner));
		}
		
		
		if ($id_topic = self::getRequestedTopics()) {
			
			$realm->sql_criteria = $realm->sql_criteria->_AND_($realm->id_topic->in($id_topic));
		}
		

		$a_authorid = (int) bab_rp('a_authorid');
		if ($a_authorid) {
			$realm->sql_criteria = $realm->sql_criteria->_AND_($realm->id_author->is($a_authorid));
		}

		include_once $GLOBALS['babInstallPath'].'utilit/dateTime.php';
		if ($after = BAB_DateTime::fromUserInput(bab_rp('after'))) {
			$realm->sql_criteria = $realm->sql_criteria->_AND_($realm->date_publication->greaterThanOrEqual($after->getIsoDateTime()));
		}

		if ($before = BAB_DateTime::fromUserInput(bab_rp('before'))) {
			$before->add(1, BAB_DATETIME_DAY);
			$realm->sql_criteria = $realm->sql_criteria->_AND_($realm->date_publication->lessThan($before->getIsoDateTime()));
		}


		return $criteria;
	}
}