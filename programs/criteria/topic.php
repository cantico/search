<?php
/************************************************************************
 * OVIDENTIA http://www.ovidentia.org                                   *
 ************************************************************************
 * Copyright (c) 2003 by CANTICO ( http://www.cantico.fr )              *
 *                                                                      *
 * This file is part of Ovidentia.                                      *
 *                                                                      *
 * Ovidentia is free software; you can redistribute it and/or modify    *
 * it under the terms of the GNU General Public License as published by *
 * the Free Software Foundation; either version 2, or (at your option)  *
 * any later version.													*
 *																		*
 * This program is distributed in the hope that it will be useful, but  *
 * WITHOUT ANY WARRANTY; without even the implied warranty of			*
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.					*
 * See the  GNU General Public License for more details.				*
 *																		*
 * You should have received a copy of the GNU General Public License	*
 * along with this program; if not, write to the Free Software			*
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,*
 * USA.																	*
************************************************************************/



class search_TopicCriteriaFactory extends search_CriteriaFactory
{
    

    /**
     * Get requested topics from drop down list
     * @return array
     */
    protected static function getRequestedTopics() {
    
        $return = array();
        $a_topiccategory = bab_rp('a_topiccategory');
    
        if (trim($a_topiccategory) != "") {
    
            $id_category = false;
            $id_topic = false;
    
            if (false !== mb_strpos($a_topiccategory, 'category-')) {
                $id_category = (int) mb_substr($a_topiccategory, strlen('category-'));
            }
    
            if (false !== mb_strpos($a_topiccategory, 'topic-')) {
                $id_topic = (int) mb_substr($a_topiccategory, strlen('topic-'));
            }
    
            if ($id_topic) {
                $return[] = $id_topic;
            }
    
    
            if ($id_category) {
                include_once $GLOBALS['babInstallPath'].'utilit/topincl.php';
                $return = bab_getTopicsFromCategory($id_category);
            }
        }
    
        // list only allowed topics
        if ($return)
        {
            $topview = bab_getUserIdObjects(BAB_TOPICSVIEW_GROUPS_TBL);
            $return = array_intersect($return, $topview);
        }
    
        return $return;
    }
    
    

    /**
     * get a criteria from a search query made with the form generated with the method <code>getSearchFormHtml()</code>
     * @see Func_SearchUi::getSearchFormHtml()
     * @return bab_SearchCriteria
     */
    public function getSearchFormCriteria() {
        
        $realm = $this->realm;
        
        // default search fields
        $criteria = search_DefaultForm::getCriteria($realm);
    
        if ($topics = self::getRequestedTopics()) {
            $criteria = $criteria->_AND_($realm->id_topic->in($topics));
        } else {
            $criteria = $criteria->_AND_($realm->id_topic->in(bab_getUserIdObjects(BAB_TOPICSVIEW_GROUPS_TBL)));
        }
    
    
        $a_authorid = (int) bab_rp('a_authorid');
        if ($a_authorid) {
            $criteria = $criteria->_AND_($realm->id_author->is($a_authorid));
        }
    
        include_once $GLOBALS['babInstallPath'].'utilit/dateTime.php';
        if ($after = BAB_DateTime::fromUserInput(bab_rp('after'))) {
            $criteria = $criteria->_AND_($realm->date_publication->greaterThanOrEqual($after->getIsoDateTime()));
        }
    
        if ($before = BAB_DateTime::fromUserInput(bab_rp('before'))) {
            $before->add(1, BAB_DATETIME_DAY);
            $criteria = $criteria->_AND_($realm->date_publication->lessThan($before->getIsoDateTime()));
        }
    
    
        return $criteria;
    }
}