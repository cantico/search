<?php
/************************************************************************
 * OVIDENTIA http://www.ovidentia.org                                   *
 ************************************************************************
 * Copyright (c) 2003 by CANTICO ( http://www.cantico.fr )              *
 *                                                                      *
 * This file is part of Ovidentia.                                      *
 *                                                                      *
 * Ovidentia is free software; you can redistribute it and/or modify    *
 * it under the terms of the GNU General Public License as published by *
 * the Free Software Foundation; either version 2, or (at your option)  *
 * any later version.													*
 *																		*
 * This program is distributed in the hope that it will be useful, but  *
 * WITHOUT ANY WARRANTY; without even the implied warranty of			*
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.					*
 * See the  GNU General Public License for more details.				*
 *																		*
 * You should have received a copy of the GNU General Public License	*
 * along with this program; if not, write to the Free Software			*
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,*
 * USA.																	*
************************************************************************/

class search_DirectoriesCriteriaFactory extends search_CriteriaFactory
{
    /**
     * get a criteria from a search query made with the form generated with the method <code>getSearchFormHtml()</code>
     * @see Func_SearchUi::getSearchFormHtml()
     * @return bab_SearchCriteria
     */
    public function getSearchFormCriteria()
    {
        $realm = $this->realm;
        
        $directoryid = (int) bab_rp('directoryid');
        if ($directoryid) {
            $realm->setDirectory($directoryid);
        }
        
        // default serach fields
        $criteria = search_DefaultForm::getCriteria($realm);
        
        $select = bab_rp('f');
        $values = bab_rp('v');
        
        if ($select && is_array($select)) {
            foreach($select as $key => $customfield) {
                if (isset($realm->$customfield) && !empty($values[$key])) {
                    $criteria = $criteria->_AND_($realm->$customfield->contain($values[$key]));
                }
            }
        }
        
        return $criteria;
    }
}
