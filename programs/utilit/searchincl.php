<?php
/************************************************************************
 * OVIDENTIA http://www.ovidentia.org                                   *
 ************************************************************************
 * Copyright (c) 2003 by CANTICO ( http://www.cantico.fr )              *
 *                                                                      *
 * This file is part of Ovidentia.                                      *
 *                                                                      *
 * Ovidentia is free software; you can redistribute it and/or modify    *
 * it under the terms of the GNU General Public License as published by *
 * the Free Software Foundation; either version 2, or (at your option)  *
 * any later version.													*
 *																		*
 * This program is distributed in the hope that it will be useful, but  *
 * WITHOUT ANY WARRANTY; without even the implied warranty of			*
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.					*
 * See the  GNU General Public License for more details.				*
 *																		*
 * You should have received a copy of the GNU General Public License	*
 * along with this program; if not, write to the Free Software			*
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,*
 * USA.																	*
************************************************************************/

/**
 * @package search
 */
class search_DefaultForm {



	/**
	 * default search form
	 * @return string
	 */
	public static function getHTML() {

		$options = array(
			'OR' 	=> search_translate('Or'),
			'AND'	=> search_translate('And'),
			'NOT'	=> search_translate('Exclude')
		);

		$htmloptions = '';

		foreach($options as $key => $value) {
			if ($key === bab_rp('option')) {
				$option = '<option value="%option%" selected="selected">%title%</option>';
			} else {
				$option = '<option value="%option%">%title%</option>';
			}

			$htmloptions .= str_replace(
				array('%option%'	, '%title%'),
				array($key			, $value),
				$option
			);
		}

		global $babBody;
		$babBody->addJavascriptFile($GLOBALS['babScriptPath'].'search.js');

		return str_replace(
			array('%labelprimary%'			, '%labelsecondary%'				, '%primary%'					, '%htmloptions%'	, '%secondary%'),
			array(search_translate("Search")	, search_translate("Advanced search")	, bab_toHtml(bab_rp('what'))	, $htmloptions		, bab_toHtml(bab_rp('what2'))),
			'
			<p id="bab_search_primary_bloc">
				<label for="bab_search_primary">%labelprimary% :</label>
				<input type="text" id="bab_search_primary" name="what" size="40" value="%primary%" />
			</p>

			<p id="bab_search_secondary_bloc">

				<select name="option">
					%htmloptions%
				</select>
				<input type="text" id="bab_search_secondary" name="what2" size="30" value="%secondary%" />
			</p>
			'
		);
	}


	/**
	 * Add criterions from default search form
	 * @return bab_SearchCriteria
	 */
	private static function addFromCriterions(bab_SearchCriteria $crit, bab_SearchTestable $testable) {



		$primary_search = bab_rp('what');
		$secondary_search = bab_rp('what2');
		$option = bab_rp('option');
		$delegation = bab_rp('delegation', null);




		if ($primary_search) {

			$primary_search_criteria = self::searchStringToCriteria($testable, $primary_search);

			if (!($primary_search_criteria instanceOf bab_SearchInvariant)) {
				$crit = $crit->_AND_($primary_search_criteria);
			}
		}

		if ($secondary_search) {


			$secondary_search_criteria = self::searchStringToCriteria($testable, $secondary_search);

			if (!($secondary_search_criteria instanceOf bab_SearchInvariant)) {
				switch($option) {

					case 'AND':
						$crit = $crit->_AND_($secondary_search_criteria);
						break;

					case 'NOT':
						$crit = $crit->_AND_($secondary_search_criteria->_NOT_());
						break;

					case 'OR':
					default:
						$crit = $crit->_OR_($secondary_search_criteria);
						break;
				}
			}
		}


		if (null !== $delegation && ($testable instanceOf bab_SearchRealm) && isset($testable->id_dgowner) && 'DGAll' !== $delegation)
		{
			// if id_dgowner field exist on search real, filter by delegation

			require_once $GLOBALS['babInstallPath'].'utilit/delegincl.php';
			$arr = bab_getUserVisiblesDelegations();

			if (isset($arr[$delegation]))
			{
				$id_dgowner = $arr[$delegation]['id'];
				$crit = $crit->_AND_($testable->id_dgowner->is($id_dgowner));
			}
			$id_dgowner = str_replace('DG', '', $delegation);
			$crit = $crit->_AND_($testable->id_dgowner->is($id_dgowner));
		}


		return $crit;
	}


	



	/**
	 * add default search form criteria to testable object
	 * the criteria is generated from the default form search
	 *
	 * @param	bab_SearchTestable $testable		search realm or search field
	 * @return 	bab_SearchCriteria
	 */
	public static function getCriteria(bab_SearchTestable $testable) {

		$crit = new bab_SearchInvariant;
		
		$criteria = self::addFromCriterions($crit, $testable);
		
		if ($testable instanceOf bab_SearchRealm) {
			$x = $testable->getDefaultCriteria();
			
			if (!($x instanceof bab_SearchInvariant))
			{
				$criteria = $criteria->_AND_($x);
			}
		} 
		
		
		return $criteria;
	}






	/**
	 * create a criteria for search queries without fields (search file content)
	 * the criteria is generated from the default form search
	 *
	 * @param	bab_SearchRealm 	$realm
	 * @return 	bab_SearchCriteria
	 */
	public static function getFieldLessCriteria(bab_SearchRealm $realm) {

		$crit = new bab_SearchInvariant;

		if (!isset($realm->search)) {
			return $crit;
		}

		return self::addFromCriterions($crit, $realm->search);
	}





	/**
	 * Create a <code>bab_searchCriteria</code> from a string of the search form
	 * used for primary_search and secondary_search
	 *
	 * @param	bab_searchTestable	$testable
	 * @param	string				$search
	 * @param	string				$operator
	 * @return bab_searchCriteria
	 */
	public static function searchStringToCriteria($testable, $search, $operator = '_AND_') {

		$criteria = new bab_SearchInvariant;

		if (preg_match_all('/(?:([^"][^\s]+)|(?:"([^"]+)")|(\w))\s*/', $search, $matchs)) {

			$arr = array();

			foreach($matchs[1] as $key => $match) {
				if (trim($match)) {
					$arr[] = trim($match);
				}
				if (trim($matchs[2][$key])) {
					$arr[] = trim($matchs[2][$key]);
				}

				if (trim($matchs[3][$key])) {
					$arr[] = trim($matchs[3][$key]);
				}
			}


			foreach($arr as $keyword) {

				$keyword = trim($keyword, ' ,;.');

				if ($keyword) {
					$criteria = $criteria->$operator($testable->contain($keyword));
				}
			}
		}

		return $criteria;
	}

}





class search_CriteriaFactory
{
    /**
     * @var bab_SearchRealm
     */
    protected $realm;
    
    public function __construct(bab_SearchRealm $realm)
    {
        $this->realm = $realm;
    }
}
