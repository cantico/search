��    J      l  e   �      P     Q     _     o  
   u     �     �     �     �     �     �     �     �     �     �     �     �     �                         &     +  
   =     H     X     d     m     s     z  &   �  
   �     �     �     �     �     �  	   �     �     �     �  	     
        (     1     =     B     G     e     t     �     �     �     �  	   �     �     �     �     �     �     �     �     �     	     3	     J	     O	  )   W	     �	     �	     �	  %   �	     �	  8  �	          +     >     E     T     Y     \     p     w     }     �  
   �     �     �     �     �     �     �     �  	   �                    &     3     C     O     `     f     m  5   u     �     �  !   �     �     �     �     �       
             +     /     <     E     R     V  &   ^     �     �     �     �     �     �  
   �     �     �     �     �  
     
     $   "  $   G  #   l     �     �     �  3   �     �     �  #   �  $        C     !          
      ?       1       F   G      <      8             .       2      %           H       @      #                 J          /   -       $       "   D   9   A   :       C           ;      5   E   &          I            7   ,   4                 6      	       )   >                          0      '   3   (           B             *         +            =    Access denied Advanced search After After date All And Associated documents Author Before Before date Business Address Business Fax Business Tel By Calendar Cancel Change search... Close Comments Compagny Created Date Default interface Delegation Dependent files Description Download Email Emails Exclude Filter search results by a directory : First Name From Hide search form Home Address Home Tel In Job Title Kb Keywords Keywords of the thesaurus Last Name Mobile Tel Modified Modified by Name Next Number of results in research Older versions Open in a popup Or Page Pages Path Posted by Previous Private folder Result Result in file Results Search Search in a specific field : Search page with %d results Search page with one result Search result is empty Size Subject The dates search boundaries are mandatory To Topic You do not have access rights You don't have access to this contact Your search is empty Project-Id-Version: search
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2014-11-14 13:44+0100
PO-Revision-Date: 2014-11-14 13:46+0100
Last-Translator: de Rosanbo Paul <paul.derosanbo@cantico.fr>
Language-Team: Cantico <paul.derosanbo@cantico.fr>
Language: fr_FR
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n > 1);
X-Poedit-SourceCharset: iso-8859-1
X-Poedit-Basepath: ../
X-Poedit-KeywordsList: search_translate;search_translate:1,2
X-Generator: Poedit 1.5.4
X-Poedit-SearchPath-0: programs
 Accès refusé Recherche avancée Après Après la date Tous Et Documents associés Auteur Avant Avant la date Adresse professionnelle Fax bureau Tél. bureau Par Agenda Annuler Modifier la recherche... Fermer Commentaires Société Crée Date Interface par défaut Délégation Fichiers joints Description Téléchargement Email Emails Exclure Filtrer les résultats de recherche par un annuaire : Prénom Du Cacher le formulaire de recherche Adresse domicile Tél. personnel Dans Titre Ko Mots clés Mots clés du thésaurus Nom Tél. mobile Modifié Modifié par Nom Suivant Nombre de résultats dans la recherche Anciennes versions Ouvrir dans une popup Ou Page Pages Chemin Posté par Précédent Dossier privé Résulat Résultat dans le fichier Résultats Rechercher Chercher dans un champ spécifique : Page de recherche avec %d résultats Page de recherche avec un résultat Résultat de la recherche vide Taille Sujet Les dates limites de la recherche sont obligatoires Au Thème Vous n'avez pas les droits d'accès Vous n'avez pas accès à ce contact Votre recherche est vide 